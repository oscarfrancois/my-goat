// Pour attraper les changements d'orientation on attache un event à la fenêtre
window.addEventListener("deviceorientation", changeOrientation, true);

let charge = true;

// A chaque changement d'orientation
function changeOrientation(event) {
  const absolute = event.absolute;
  const alpha = event.alpha;
  const beta = event.beta;
  const gamma = event.gamma;
  if (beta < 20)
	  charge = true;
  if (beta > 160 && charge){
	  charge = false;
	  appuieChevre();
  }
  console.log(absolute + " " + alpha + " " + beta + " " + gamma);
}

// Pour que lorsqu'on clique sur la chèvre elle fasse meeeh on ajoute un event sur l'image
let chevre = document.getElementById("goat");
chevre.addEventListener("click", appuieChevre, true);

let timer = null;

// A chaque fois qu'on click sur la chèvre elle fait le bruit et il y a une animation en CSS
function appuieChevre(){
	if (timer == null){
		chevre.src = "goat_meh.jpg";
		chevre.style.animation = "cerclelumineux 0.2s infinite";
		var audio = new Audio('meeeh.mp3');
		audio.loop = false;
		audio.play(); 
		timer = setTimeout(finSon, 1200);
	}
}

// Lorsque le son se termine on remet la chèvre bouche fermée et on arrête l'animation
function finSon(){
	chevre.src = "goat.jpg";
	chevre.style.animation = "";
	timer = null;
}